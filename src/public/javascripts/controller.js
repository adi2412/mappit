'use strict';

function IndexCtrl($scope, $http){
	$http.get('/api/pins').
		success(function(data, status, headers, config){
			$scope.posts = data.posts;
		});
}

function AddPinCtrl($scope, $http, $location){
	$scope.form = {};
	$scope.submitPin = function() {
		$http.post('/api/pin', $scope.form).
			success(function(data){
				$location.path('/');
			});
	};
}

