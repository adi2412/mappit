'use strict';

angular.module('myApp', []).
	config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
		$routeProvider.
			when('/',{
				templateUrl: 'partials/index',
				controller: IndexCtrl
			}).
			when('/addPost',{
				templateUrl: 'partials/addPost',
				controller: AddPostCtrl
			}).
			when('/editPost/:id',{
				templateUrl: '/partials/editPost',
				controller: EditPostCtrl
			}).
			when('/deletePost/:id',{
				templateUrl: '/partials/deletePost',
				controller: DeletePostCtrl
			}).
			otherwise({
				redirectTo: '/'
			});
		$locationProvider.html5Mode(true);
	}]);

angular.module('mapApp', []).
	config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
		$routeProvider.
			when('/',{
				templateUrl: 'partials/index',
				controller: IndexCtrl
			}).
			when('/addPin',{
				templateUrl: 'partials/addPin',
				controller: AddPinCtrl
			}).
			otherwise({
				redirectTo: '/'
			});
		$locationProvider.html5Mode(true);
	}]);
	