/*var mapboxApp = angular.module('mapboxApp', []);

mapboxApp.controller('MapBoxCtrl', function MapBoxCtrl($scope){
	$scope.phones = [
		{ 'latlng' : '37.9,-77',
			'name' : 'Pin One', 'author' : 'aditya', 'description' : 'blah blah'}
		{ 'latlng' : '40.6,-75',
			'name': 'Pin two', 'author' : 'anurag', 'description' : 'bamboo'}
	];
});*/
'use strict';

function IndexCtrl($scope, $http){
	$http.get('/api/posts').
		success(function(data, status, headers, config){
			$scope.posts = data.posts;
		});
}

function AddPostCtrl($scope, $http, $location){
	$scope.form = {};
	$scope.submitPost = function() {
		$http.post('/api/post', $scope.form).
			success(function(data){
				$location.path('/');
			});
	};
}

function EditPostCtrl($scope, $http, $location, $routeParams){
	$scope.form = {};
	$http.get('/api/post/' + $routeParams.id).
		success(function(data){
			$scope.form = data.post;
		});

	$scope.editPost = function(){
		$http.put('/api/post/' + $routeParams.id, $scope.form).
			success(function(data){
				$location.url('/readPost/' + $routeParams.id);
			});
	};
}

function DeletePostCtrl($scope, $http, $location, $routeParams){
	$http.get('/api/post/' + $routeParams.id).
		success(function(data){
			$scope.post = data.post;
		});

		$scope.deletePost = function() {
			$http.delete('/api/post/' + $routeParams.id).
				success(function(data){
					$location.url('/');
				});
		};

		$scope.home = function(){
			$location.url('/');
		};
}