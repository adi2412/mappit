//var pins = mapPins;
var map, layer = null;

var pins = [];

var map = L.mapbox.map('map', 'adi2412.map-fybvdwpq',{
            minZoom: 4,
            maxBounds: [[-85.03594,-174.97812],[85.03594,174.97812]]
          })
          .setView([37.9, -77], 5
          );

var layer = L.mapbox.tileLayer('adi2412.map-tjpij93k');

var zoomControls = L.control.zoom();

zoomControls.setPosition("topright");

zoomControls.addTo(map);

//Add all the pins from MapPins

function addPins(){
  jQuery.get("/api/pins/", function (data, textStatus, jqXHR) {
    pins = data.posts;
    console.log(pins[0].lat);
    for(var i = 0; i< 2; i++){
    L.marker([pins[i].lat,pins[i].lng]).addTo(map)
      .bindPopup(pins[i].title)
      .on('click', function(e){
        updatePin(e.target._leaflet_id);
      });
    } 
  });  
}

function updatePin(id) {
  //var pin = $.grep(pins, function(e){ return e._leaflet_id == id;});
  jQuery.get("/api/pin/" + id, function (data, textStatus, jqXHR){
    pin = data.post;
    console.log(pin);
    $('.pin-title').html(pin[0].title) ;
    $('.pin-description').html(pin[0].description) ;
    $('.pin-author').html(pin[0].author) ;
  });
}

function onMapClick(latlng) {
  if(longpress){
    var newPin = {'lat': latlng.lat, 'lng': latlng.lng,
                  'title': 'New Pin' , 'author': 'user', 'description' : 'new added pin'};
    var marker = L.marker(latlng).addTo(map)
        .bindPopup("You clicked the map here!")
        .openPopup()
        .on('click',function(e){
          updatePin(e.target._leaflet_id);
        });
    id = marker._leaflet_id;
    // addPin(newPin._leaflet_id);
    $(location).attr('href','/addPost');
    console.log(id);
    $('.pinCreate #lat').val(latlng.lat);
    $('.pinCreate #lng').val(latlng.lng);
    $('.pinCreate #leaflet_id').val(id);
    //mapPins.push(newPin);
    longpress = false;
    //updatePin(newPin._leaflet_id);   
  }
}

function addPin(id){
  //$('#pin-info').css('display', 'none');
  //$('#pin-create').css('display', 'block');

}

addPins();

var longpress = false;
//Implementing long click calls
function testLongClick(e){
  console.log(e.latlng.toString());
  var latln = e.latlng;
  if(longpress)
    onMapClick(latln);
}

map.on('click', testLongClick);

var startTime, endTime;

$("#map").on('mousedown', function(){
  startTime = new Date().getTime();
});

$("#map").on('mouseup', function(){
  endTime = new Date().getTime();
  longpress = (endTime - startTime < 500)? false : true;
});