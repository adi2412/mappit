
/**
 * Module dependencies.
 */

var express = require('express')
  , user = require('./routes/http/users')
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose')
  , engine = require('ejs-locals')
  , expressMongoose = require('express-mongoose');

var app = express();
app.engine('ejs', engine);

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
  app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));


var connection = 'mongodb://localhost/test';
mongoose.connect(connection);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

require('./routes/controller')(app);

/*app.get('/', routes.index);
app.get('/test', routes.test);
app.get('/users', user.list);
app.get('/user', routes.user);
app.get('/createUser', routes.createUser);
app.get('/showUser', routes.showUser);*/

http.createServer(app).listen(app.get('port'),'0.0.0.0', function(){
  console.log('Express server listening on port ' + app.get('port'));
});
