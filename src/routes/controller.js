var routes = require('./http');

var myRoutes = [
{ url: '/', type: 'get', route: routes.index},
{ url: '/partials/:name', type: 'get', route: routes.partials},
{ url: '/test', type: 'get', route: routes.test},
{ url: '/user', type: 'get', route: routes.user},
{ url: '/users', type: 'get', route: routes.users.list},
{ url: '/createUser', type: 'get', route: routes.createUser},
{ url: '/showUser', type: 'get', route: routes.showUser},
{ url: '/deleteUsers', type: 'get', route: routes.deleteUsers},
{ url: '/showUsers', type: 'get', route: routes.showUsers},
{ url: '/map', type: 'get', route: routes.map},
{ url: '/api/posts', type: 'get', route: routes.api.posts},
{ url: '/api/post/:id', type: 'get', route: routes.api.post},
{ url: '/api/post', type: 'post', route: routes.api.addPost},
{ url: '/api/post/:id', type: 'put', route: routes.api.editPost},
{ url: '/api/post/:id', type: 'delete', route: routes.api.deletePost},
{ url: '/api/pins', type: 'get', route:routes.api.pins},
{ url: '/api/pin/:id', type: 'get', route: routes.api.pin},
{ url: '/api/pin', type: 'post', route: routes.api.addPin}
];

module.exports = function(app) {
	myRoutes.forEach(function(route){
		if(route && route.type && route.url && route.route) {
			app[route.type](route.url, route.route);
		}
	});
};