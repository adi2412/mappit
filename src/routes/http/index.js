
/*
 * GET home page.
 */
require('express-mongoose');

var mongoose = require('mongoose');

var User = require('../../lib/models/user');
var Pin = require('../../lib/models/pin');

//Export all the other routing files in http here.
exports.users = require('./users');
exports.api = require('./api');

exports.index = function(req, res){
  res.render('index', { title: 'Express' });
};

exports.partials = function(req,res){
	var view = req.params.name;
	res.render("partials/" + view);
}

exports.test = function(req,res){
	res.render('test', {title: 'Test',pin: {
  "title": "Name of pin title comes here",
  "description": "Description of pin comes here",
  "Author": "Author of pin comes here"
}});
	//res.render('test',{title: 'Test'});
};

exports.user = function(req,res) {
	var anurag = new User({ first: 'Anurag', email: 'aprasad1993@gmail.com'});
	anurag.save(function(err,callback){});
	//var result = anurag.speak();
	//return res.send(result);

	res.render('user', {
		result: anurag.speak(),
		title: "Testing"
	});

};

exports.createUser = function(req,res){
	var user = User.createUser('Aditya','aditya.ajeet@gmail.com');
	res.send(user);
}

exports.showUser = function(req,res){
	User.findOne({ first: 'Aditya'}).exec(function(err,doc){
		if(err){
			res.send("Cannot display user because of error:" + err);
		}
		else if(doc != null){
			res.send(doc);
			console.log(doc);
		}
		else
			res.send("No such user exists!");
	});
}

exports.showUsers = function(req,res){
	User.find({}).exec(function(err,docs){
		if(err){
			res.send("Cannot display user because of error:" + err);
		}
		else if(docs != null){
			res.render('showusers', {title: 'All Users', users: docs});
		}
		else
			res.send("No users exist");
	});
}

exports.deleteUsers = function(req,res){
	User.remove({}, function(err){
		if(err){
			res.send(err);
		}
		else{
				res.send("All Users deleted");
		}
	});
}

exports.map = function(req,res){
/*	Pin.create({lat: 37.9, lng: -77, leaflet_id: 27, title: 'Pin One', author: 'aditya', description: 'blah'},
							{lat: 40.6, lng: -75, leaflet_id: 31, title: 'Pin Two', author: 'anurag', description: 'bamboo'}, function(err,doc){
								if(err)
									throw err;
							})*/
	Pin.find({}).exec(function(err,docs){
		if(err)
			throw err;
		else if(docs != null){
			res.render('map', {title: 'Map', posts: docs});
		}
		else{
			res.send("No pins exist");
		}
	})
/*	res.render('map', {title: 'Map',pin: {
  "title": "Name of pin title comes here",
  "description": "Description of pin comes here",
  "Author": "Author of pin comes here"
	}});*/
}

exports.addPin = function(req,res){
	res.render('partials/addPin');
}