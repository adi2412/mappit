require('express-mongoose');

var mongoose = require('mongoose');

var User = require('../../lib/models/user');
var Pin = require('../../lib/models/pin');

// GET

exports.posts = function(req, res){
	var posts = [];
	User.find({}).exec(function(err,docs){
		if(err){
			throw err;
		}
		else if(docs != null){
			res.send({ posts: docs})
		}
		else
			res.send("No users exist");
	});	
}

exports.post = function(req,res){
	var id = req.params.id;
	User.findById(id, function(err,user){
		if(user){
			res.json({post: user});
		}
		else{
			res.json("No such user found");
		}
	});
}

exports.addPost = function(req,res){
	//mongoose new object
	User.create({first: req.body.first, email: req.body.email}, function(err,user){
		if(err)
			throw err;
		else{
			res.json(req.body);
		}
	});
}

exports.editPost = function(req,res){
	var id = req.params.id;
	User.findByIdAndUpdate(id,{first:req.body.first, email:req.body.email},function(err,user){
		if(err)
			throw err;
		else if(user)
			res.json(true);
		else
			res.json(false);
	})
}

exports.deletePost = function(req,res){
	var id = req.params.id;
	User.findByIdAndRemove(id,function(err){
		if(err)
			throw err;
		else
			res.json(true);
	});
}

exports.pins = function(req,res){
	Pin.find({}).exec(function(err,pins){
		if(err){
			throw err;
		}
		else if(pins != null){
			res.send({posts: pins});
		}
		else{
			res.send("No users exist");
		}
	});
}

exports.pin = function(req,res){
	var id = req.params.id;
	Pin.find({leaflet_id: id}, function(err,pin){
		if(pin){
			res.send({post: pin});
		}
		else{
			res.send("No such pin found");
		}
	});
}

exports.addPin = function(req,res){
	Pin.create({title: req.body.title, author: req.body.author, description: req.body.description, lat: req.body.lat,
							lng: req.body.lng, leaflet_id: req.body.leaflet_id}, function(err,user){
		if(err)
			throw err;
		else{
			res.json(req.body);
		}
	});
}




