var mongoose = require('mongoose'),
		Schema = mongoose.Schema;


var UserSchema = new Schema({
	first: { type: String, required: true},
	email: { type: String, required: false},
});

UserSchema.methods.speak = function(){
	var greeting = this.first
	? "Meow name is " + this.first
	: "I don't have a name"
	console.log(greeting);
	return greeting;
}

UserSchema.statics.createUser = function(firsts, emails){
	var user = new User({ email: emails, first: firsts});
	user.save(function(err,callback){
		console.log(user);
	});
	return user;
}

/*UserSchema.statics.showUser = function(){
	(UserSchema.find({first : 'Anurag'}, function(err,docs){
		console.log(docs);
	}));
}*/


var User = mongoose.model('User', UserSchema);

module.exports = User;