var mongoose = require('mongoose'),
		Schema = mongoose.Schema;


var PinSchema = new Schema({
	title: { type: String, required: true},
	description: { type: String, required: false},
	author: { type: String, required: false},
	lat: { type: Number, required: true},
	lng: { type: Number, required: true},
	leaflet_id: { type: Number, required: true},
});

/*PinSchema.statics.createUser = function(firsts, emails){
	var user = new User({ email: emails, first: firsts});
	user.save(function(err,callback){
		console.log(user);
	});
	return user;
}*/

/*UserSchema.statics.showUser = function(){
	(UserSchema.find({first : 'Anurag'}, function(err,docs){
		console.log(docs);
	}));
}*/


var Pin = mongoose.model('Pin', PinSchema);

module.exports = Pin;