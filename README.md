This is a first try for the app Mappit.

How to run the app

To set up the app

run make all
To install all dependencies and make the deployment directory.

REMEMBER: Use only lib to run the app.

To run the app

cd ./lib
node app.js


REMEMBER: Make all changes in the src folder only and the run
make install_server

To build the working directory


Also, if you're making any changes create your own branch first.

git checkout -b branch-name

add your changes to the stage

git add .

commit them

git commit -m "Commit message"

Then merge to the master branch.

git checkout master

git merge branch-name

Optional: If you feel you're done with that branch, delete if using the delete option

git branch -d branch-name