all: install_dependencies install_server

clean:
	rm -rf ./lib

install_server:
	mkdir -p ./lib
	rsync -r --exclude=.svn ./src/ ./lib

install_dependencies: install_server
	cd . && npm install

run_server:
	cd lib && node app.js

build_server: install_server run_server